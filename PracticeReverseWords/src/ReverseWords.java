import java.io.*;
import java.util.Scanner;


public class ReverseWords {

	//Reads in the input from the file and reverses the words for each line

		public static void main(String[] args) throws IOException 
		{
			//Declare variables we will be using and open the input file
			int NCases, size;
			String sentence;
			Scanner input = new Scanner(new File("B-small-practice.in"));
			
			//Checks to see if the output file is already created, if it is, 
			//then delete and remake it to remove old data, else create it
			File fileOut = new File("reverseWordsSmallOutput.txt");
		    if( !fileOut.createNewFile())
		    {
		    	fileOut.delete();
		     	fileOut.createNewFile();
		    }
		    
		    //Make the output file write-table to  
		    FileWriter fw = new FileWriter(fileOut,true);
		    		
			//Get the first number in the file, which is the number of cases we will be dealing
			//with and convert it to and int
			String[] singleNum = input.nextLine().split(" ");
			NCases = Integer.parseInt(singleNum[0]);
			
			for (int i=0; i<NCases; i++)
			{
				//Get the sentence we will be dealing with to reverse
				sentence = input.nextLine();
				
				//Parse through the sentence to split each individual word up and save the  
				//number of words in the array
				String[] tokens = sentence.split(" ");
				size = tokens.length - 1;
				
				//Write out the words from the sentence in reverse 
				fw.write("Case #" + (i+1)  + ": ");
				for (int current = 0; current < tokens.length; current++ )
				{
					if ( size - current == 0)
						fw.write( tokens[size - current] );
					else
						fw.write( tokens[size - current] + " ");
				}
				fw.write( "\n" );					
			}
			
			//closes the input file we read the data from as well as the file we wrote out to
			input.close();
			fw.close();
		}
	}
