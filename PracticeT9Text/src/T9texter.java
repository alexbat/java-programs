import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class T9texter {

	//Reads in the input from the file and translates it into T9 text, the buttons which need to
	//be pushed for the text to be printed out

		public static void main(String[] args) throws IOException 
		{
			//Declare variables we will be using, create the dictionary/mapper
			//for the character to T9 and open the input file
			int NCases;
			Map<String, String> map = new HashMap<String, String>();
			String line, previous;
			Scanner input = new Scanner(new File("C-small-practice.in"));
			
			//Checks to see if the output file is already created, if it is, 
			//then delete and remake it to remove old data, else create it
			File fileOut = new File("T9SmallOutput.txt");
		    if( !fileOut.createNewFile())
		    {
		    	fileOut.delete();
		     	fileOut.createNewFile();
		    }
		    
		    //Make the output file write-table to  
		    FileWriter fw = new FileWriter(fileOut,true);
		    		
			//Get the first number in the file, which is the number of cases we will be dealing
			//with and convert it to and int
			String[] singleNum = input.nextLine().split(" ");
			NCases = Integer.parseInt(singleNum[0]);
			
			//Create the dictionary with the characters we will switch with
			loadDictionary(map);
			
			for (int i=0; i<NCases; i++)
			{
				//Get the sentence we will be dealing with to reverse and parse through the sentence  
				// to split each individual word up and save them in the array
				line = input.nextLine();
				String[] letters = line.split("(?!^)");
								
				//Write out the T9 version of each character to the output file
				fw.write("Case #" + (i+1)  + ": ");
				for (int current = 0; current < letters.length; current++ )
				{
					if( current == 0 )
						fw.write( map.get(letters[current]) );
					else
					{
						//First check to see if a space needs to be printed first if they are on the 
						//same digit, else just print the T9 of the character normally
						previous = letters[current-1];
						if( checkPreviousChar( previous, letters[current]) )
						{
							fw.write( map.get("equal") );
							fw.write( map.get(letters[current]) );
						}
						else
							fw.write( map.get(letters[current]) );
					}					
				}
				fw.write( "\n" );					
			}
						
			//closes the input file we read the data from as well as the file we wrote out to
			input.close();
			fw.close();
		}
		
		private static boolean checkPreviousChar( String a, String b)
		{
			String check1 = "abc", check2 = "def", check3 = "ghi", check4 = "jkl";
			String check5 = "mno", check6 = "pqrs", check7 = "tuv", check8 = "wxyz";
			String space = " ";
			
			if( check1.contains(a) && check1.contains(b))
				return true;
			else if( check2.contains(a) && check2.contains(b))
				return true;
			else if( check3.contains(a) && check3.contains(b))
				return true;
			else if( check4.contains(a) && check4.contains(b))
				return true;
			else if( check5.contains(a) && check5.contains(b))
				return true;
			else if( check6.contains(a) && check6.contains(b))
				return true;
			else if( check7.contains(a) && check7.contains(b))
				return true;
			else if( check8.contains(a) && check8.contains(b))
				return true;
			else if( a.equals(space) && b.equals(space))
				return true;
			
			return false;
		}
		
		
		private static void loadDictionary(Map<String, String> x )
		{
			x.put("a", "2");
			x.put("b", "22");
			x.put("c", "222");
			x.put("d", "3");
			x.put("e", "33");
			x.put("f", "333");
			x.put("g", "4");
			x.put("h", "44");
			x.put("i", "444");
			x.put("j", "5");
			x.put("k", "55");
			x.put("l", "555");
			x.put("m", "6");
			x.put("n", "66");
			x.put("o", "666");
			x.put("p", "7");
			x.put("q", "77");
			x.put("r", "777");
			x.put("s", "7777");
			x.put("t", "8");
			x.put("u", "88");
			x.put("v", "888");
			x.put("w", "9");
			x.put("x", "99");
			x.put("y", "999");
			x.put("z", "9999");
			x.put(" ", "0");
			x.put("equal", " ");
		}
}
